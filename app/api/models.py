from pydantic import BaseModel


class Origin(BaseModel):
    name: str
    namespace: str
    uid: str
    pod_ip: str
    node_name: str
    node_ip: str


class Event(BaseModel):
    origin: Origin
    target: str
    message: str
