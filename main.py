import logging

from fastapi import FastAPI
from fastapi.openapi.utils import get_openapi

from app.api.tags import tags_metadata
from app.api import routes

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s — %(name)s — %(levelname)s — %(message)s",
)


def custom_openapi():
    """
    Custom OpenAPI values
    """
    if app.openapi_schema:
        return app.openapi_schema
    openapi_schema = get_openapi(
        title="Policy Information Point",
        version="1.0.0",
        description="This is a Policy Information Point (PIP) dummy writen in Python with FastAPI",
        routes=app.routes,
    )
    openapi_schema["info"]["x-logo"] = {
        "url": "https://fastapi.tiangolo.com/img/logo-margin/logo-teal.png"
    }
    app.openapi_schema = openapi_schema
    return app.openapi_schema


app = FastAPI(openapi_tags=tags_metadata)

app.include_router(routes.router)
app.openapi = custom_openapi


@app.on_event("startup")
async def startup_event():
    logging.info("Application start")


@app.on_event("shutdown")
async def shutdown_event():
    logging.info("Application shutdown")
